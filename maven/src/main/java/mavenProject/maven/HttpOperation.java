package mavenProject.maven;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class HttpOperation {
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = "https://reqres.in/api";
	}
	@Test
	public void GetTestCase()
	{   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/3")
			    .then().assertThat().statusCode(200).body("data.size()", greaterThan(0)).body("data.first_name", notNullValue());
		System.out.println(res.extract().asString());
	}
	@Test
	public void RegistrationSuccessful()
	{	
		JSONObject persons = new JSONObject();
		persons.put("first_name", "Sergey");
		persons.put("job", "Kargopolov");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.post("/users")
		.then()
		.statusCode(201).body("data.first_name", equalTo("Sergey"));
		System.out.println(response.extract().asString());
	}
	@Test
	public void putTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put("first_name", "pal");
		persons.put("job", "SE");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.put("/users/3")
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		System.out.println(response.extract().asString());
	}
	@Test
	public void patchTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put("first_name", "pal");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.patch("/users/3")
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		System.out.println(response.extract().asString());
	}
	@Test
	public void testDelete(){
		given()
		.when()
		.delete("users/2")
		.then().statusCode(204);
	}
}
