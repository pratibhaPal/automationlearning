package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import mavenProject.maven.Constant;

public class PatchOperationApiTests {
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.URL;
	}
	@Test
	public void updateParticularFieldTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "pal");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.patch("/users/"+Constant.VALIDUSERID)
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		System.out.println(response.extract().asString());
	}
	@Test
	public void InvalidupdateParticularFieldTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.patch("/users/"+Constant.VALIDUSERID)
		.then()
		.statusCode(200);
		System.out.println(response.extract().asString());
	}
}
