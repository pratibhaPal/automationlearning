package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import mavenProject.maven.Constant;

public class PutOperationApiTests {
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.URL;
	}
	@Test
	public void updateUserTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "pal");
		persons.put(Constant.JOB, "SE");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.put("/users/"+Constant.VALIDUSERID)
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		System.out.println(response.extract().asString());
	}
	public void InvalidupdateUserTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "");
		persons.put(Constant.JOB, "");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.put("/users/"+Constant.VALIDUSERID)
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		System.out.println(response.extract().asString());
	}
}
