package mavenProject.maven;

public class Constant {
	public static final String URL = "https://reqres.in/api";
	public static final int VALIDUSERID = 2;
	public static final int INAUTHENTICUSERID = 1000;
	public static final String INVALIDUSERID = "dfgdf";
	public static final String FIRST_NAME = "first_name";
	public static final String JOB = "job";
}
