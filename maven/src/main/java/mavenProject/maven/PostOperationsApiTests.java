package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import mavenProject.maven.Constant;

public class PostOperationsApiTests {
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.URL;
	}
	
	@Test
	public void RegistrationSuccessful()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "Sergey");
		persons.put(Constant.JOB, "Kargopolov");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.post("/users")
		.then()
		.statusCode(201).body("data.first_name", equalTo("Sergey")).body("data.job",equalTo("Kargopolov"));
		System.out.println(response.extract().asString());
	}
	@Test
	public void nullUserTestCase()
	{	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME,"");
		persons.put(Constant.JOB, "");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.post("/users")
		.then()
		.statusCode(201);
		System.out.println(response.extract().asString());
	}
	
}
