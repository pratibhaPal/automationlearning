package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThan;
import mavenProject.maven.Constant;

public class GetUserApiTests{
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.URL;
	}
	@Test
	public void login()
	{   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/"+Constant.VALIDUSERID)
			    .then().assertThat().statusCode(200).body("data.size()", greaterThan(0));
		System.out.println(res.extract().asString());
	}
	@Test
	public void inauthenticUser()
	{   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/"+Constant.INAUTHENTICUSERID)
			    .then().assertThat().statusCode(404);
		System.out.println(res.extract().asString());
	}
	@Test
	public void invalidUser()
	{   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/"+Constant.INVALIDUSERID)
			    .then().assertThat().statusCode(404);
		System.out.println(res.extract().asString());
	}
}
