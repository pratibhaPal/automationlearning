package pomTest;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pom.pom.GmailLogin;
//import com.wordpress.Pages.LoginPageNew;
//import Helper.BrowserFactory;
 
public class GmailLoginAutomatePom
{
	@Test
	public void checkValidUser()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://www.gmail.com");
		// Created Page Object using Page Factory
		GmailLogin login_page = PageFactory.initElements(driver, GmailLogin.class);
		// Call the method
		login_page.submit("pratibha.pal62@gmail.com", "pratibha_career_25");
		
	}
}

