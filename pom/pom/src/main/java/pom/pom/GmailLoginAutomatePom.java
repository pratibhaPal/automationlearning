package pom.pom;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
 
public class GmailLoginAutomatePom {
 
	WebDriver driver;
	
	public GmailLoginAutomatePom(WebDriver ldriver){
		this.driver = ldriver;
	}
	
	@FindBy(id="identifierId") 
	WebElement emailId; 
	
	@FindBy(id="identifierNext")
	WebElement next;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(id="passwordNext")
	WebElement passwordNext;
	
	@FindBy(id=":p6")
	WebElement submit_button;
	
	public void email_submit(String email) {
		emailId.sendKeys(email);
		next.click();
	}
	public void password_submit(String pass) {
		emailId.sendKeys(pass);
		password.click();
	}
//	public void login_Gmail(String uid,String pass){
//		emailId.sendKeys(uid);
//		password.sendKeys(pass);
//		submit_button.click();
//	}
}

