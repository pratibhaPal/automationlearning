package pom.pom;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailLogin {
 
	WebDriver driver;
	
	public GmailLogin(WebDriver ldriver){
		this.driver = ldriver;
	}
	
	@FindBy(id="identifierId") 
	WebElement emailId; 
	
	@FindBy(id="identifierNext")
	WebElement next;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(id="passwordNext")
	WebElement passwordNext;
	
	@FindBy(id=":p6")
	WebElement submit_button;
	
	public void submit(String email, String pass) {
		emailId.sendKeys(email);
		next.click();
		password.sendKeys(pass);
		passwordNext.click();
	}
}

