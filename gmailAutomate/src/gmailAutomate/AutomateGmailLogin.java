package gmailAutomate;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateGmailLogin {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
	    WebDriver driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get("https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
	    // gmail login
	    driver.findElement(By.id("identifierId")).sendKeys("******88******@gmail.com");
	    driver.findElement(By.id("identifierNext")).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    driver.findElement(By.name("password")).sendKeys("**********");
	    driver.findElement(By.id("passwordNext")).click();
	    // clicks compose
	   
	    driver.findElement(By.xpath("//*[@class=\"T-I J-J5-Ji T-I-KE L3\"]")).click();
	    driver.findElement(By.id(":py")).sendKeys("pratibha.pal@intimetec.com");
	    driver.findElement(By.id(":pg")).sendKeys("This is an auto-generated mail");
	    driver.findElement(By.id(":ql")).sendKeys("This is an auto-generated mail by pratibha");
	    driver.findElement(By.id(":p6")).click();
	}

}
